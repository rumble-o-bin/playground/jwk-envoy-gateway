install-envoy:
	@kubectl apply -f https://github.com/kubernetes-sigs/gateway-api/releases/download/v1.1.0/standard-install.yaml
	@sleep 1
	@helm install eg oci://docker.io/envoyproxy/gateway-helm \
		--version v1.0.2 -n envoy-gateway-system --create-namespace \
		-f infra/envoy-gateway/values.yaml

test:
	@curl --verbose -H 'Authorization: Bearer $(BEAR) ' http://172.18.0.50/get