package main

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/lestrrat-go/jwx/jwk"
)

func main() {
	// Load private key from PEM file
	privatePEM, err := os.ReadFile("private_key.pem")
	if err != nil {
		log.Fatalf("Error reading private key: %v", err)
	}

	// Decode the PEM block
	block, _ := pem.Decode(privatePEM)
	if block == nil || block.Type != "PRIVATE KEY" {
		log.Fatalf("Failed to decode PEM block containing private key")
	}

	privateKey, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		log.Fatalf("Error parsing private key: %v", err)
	}

	// Load public key from PEM file
	publicPEM, err := os.ReadFile("public_key.pem")
	if err != nil {
		log.Fatalf("Error reading public key: %v", err)
	}

	// Decode the PEM block
	block, _ = pem.Decode(publicPEM)
	if block == nil || block.Type != "PUBLIC KEY" {
		log.Fatalf("Failed to decode PEM block containing public key")
	}

	pubKey, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		log.Fatalf("Error parsing public key: %v", err)
	}

	rsaPubKey, ok := pubKey.(*rsa.PublicKey)
	if !ok {
		log.Fatalf("Not an RSA public key")
	}

	// Convert the RSA public key to JWK
	jwkKey, err := jwk.New(rsaPubKey)
	if err != nil {
		log.Fatalf("Error creating JWK: %v", err)
	}

	// Set a key ID (kid)
	jwkKey.Set(jwk.KeyIDKey, "unique-key-id")
	jwkKey.Set(jwk.AlgorithmKey, "RS256")
	jwkKey.Set(jwk.KeyUsageKey, "sig")

	// Create a JWKS
	jwkSet := jwk.NewSet()
	jwkSet.Add(jwkKey)

	// Marshal JWKS to JSON
	jwksJSON, err := json.MarshalIndent(jwkSet, "", "    ") // Indent with 4 spaces
	if err != nil {
		log.Fatalf("Error marshaling JWKS JSON: %v", err)
	}

	jwksFile := filepath.Join("jwks.json")
	err = os.WriteFile(jwksFile, jwksJSON, 0644)
	if err != nil {
		log.Fatalf("Error writing JWKS JSON to file: %v", err)
	}
	fmt.Println("JWKS JSON written to:", jwksFile)

	// Step 2: Create and Sign JWT
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
		"sub":  "1234567890",
		"name": "John Doe",
		"iat":  time.Now().Unix(),
		"exp":  time.Now().Add(time.Hour * 24 * 14).Unix(),
	})

	tokenString, err := token.SignedString(privateKey)
	if err != nil {
		fmt.Println("Error signing JWT:", err)
		return
	}
	fmt.Println("JWT:")
	fmt.Println(tokenString)
}
